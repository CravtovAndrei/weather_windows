﻿using System;
using System.Windows.Forms;
using WeatherAPI;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;

namespace WeatherWindow
{
    public partial class Form1 : Form
    {
        WeatherInfo allWeatherInfo;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            OpenWeatherMap weather = new OpenWeatherMap();
            allWeatherInfo = new WeatherInfo();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string appID = "e7dbaa14e89101fabdd39000106b9c2d";
            string data = "Chisinau";
            string url = string.Format("http://api.openweathermap.org/data/2.5/forecast/daily?q={0}&units=metric&cnt=1&APPID={1}", data, appID);

            using(WebClient client = new WebClient())
            {
                client.UseDefaultCredentials = true;
                client.Credentials = CredentialCache.DefaultCredentials;
          //      client.Credentials = new System.Net.NetworkCredential("andrei.cravtov.ta@gmail.com", "");
                string json = client.DownloadString(url);
                allWeatherInfo = (new JavaScriptSerializer()).Deserialize<WeatherInfo>(json);
                Console.WriteLine(allWeatherInfo);
            }
        }
    }
}
