﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace WeatherAPI
{
    public class OpenWeatherMap
    {
        public string apiResponse { get; set; }

        public Dictionary<string, string> cities { get; set; }
    }
}

